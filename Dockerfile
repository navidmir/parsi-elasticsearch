FROM elasticsearch:7.17.6

COPY ./custom_plugin /usr/share/elasticsearch/custom_plugin
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install\
    file:///usr/share/elasticsearch/custom_plugin/ParsiAnalyzer-1.0-SNAPSHOT.zip
